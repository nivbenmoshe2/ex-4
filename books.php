<?php
    include "db.php";
    include "query.php";
?>
<html>
    <head>
        <title>EX 4</title>
    </head>
    <body>
        <p>
        <?php
          //database connection
           $db = new DB('localhost','intro','root','');
           $dbc = $db->connect();
           $query = new Query($dbc);
           $q = "SELECT b.title , u.name
           FROM books b , users u
           WHERE b.user_id=u.user_id";
           $result = $query->query($q);
           echo '<br>';
           if($result->num_rows > 0){
               echo '<table border=1>';
               echo '<tr><th>Book Title</th><th>User Name</th></tr>';
               while($row = $result->fetch_assoc()){
                   echo '<tr>';
                   echo '<td>'.$row['title'].'</td><td>'.$row['name'].'</td>';
                   echo '</tr>';
               }
               echo '</table>';
           }
           else {
               echo "Sorry no results";
           }
        ?>
        </P>
    </body>
</html>